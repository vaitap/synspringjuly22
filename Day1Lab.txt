Lab 7 - Hibernate (Day1.demo5.orm)
    pom file dependencies
            <!-- https://mvnrepository.com/artifact/org.springframework/spring-context -->
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context</artifactId>
                <version>5.3.21</version>
            </dependency>
            <!-- https://mvnrepository.com/artifact/org.springframework/spring-orm -->
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-orm</artifactId>
                <version>5.3.21</version>
            </dependency>
            <!-- https://mvnrepository.com/artifact/org.hibernate/hibernate-core -->
            <dependency>
                <groupId>org.hibernate</groupId>
                <artifactId>hibernate-core</artifactId>
                <version>5.6.10.Final</version>
            </dependency>
    Write demo.Dept.java
            @Entity
            @Table(name = "depttable")
            public class Dept {
                @Id
                private int deptno;
                private String dname;
                private String loc;
        + get/set + toString

    Write demo.DeptDAO
            package demo;

            import javax.annotation.PostConstruct;

            import org.hibernate.SessionFactory;
            import org.springframework.beans.factory.annotation.Autowired;
            import org.springframework.orm.hibernate5.HibernateTemplate;
            import org.springframework.stereotype.Component;
            import org.springframework.transaction.annotation.Transactional;

            @Component
            @Transactional
            public class DeptDAO {

                HibernateTemplate template;
                @Autowired
                SessionFactory sf;
                @PostConstruct
                public void init() {
                    template = new HibernateTemplate(sf);
                }
                
            public void create(Dept d) {
                System.out.println("Create invoked with " + d);
                template.save(d);
            }
            }
    Write demo.Client5

            package demo;

            import java.util.Properties;

            @Configuration
            @ComponentScan( basePackages = "demo")
            @EnableTransactionManagement
            public class Client5 {
                //Create a datasource
                @Bean
                public DataSource createds() {
                    DriverManagerDataSource ds = new DriverManagerDataSource();
                    ds.setDriverClassName("org.hsqldb.jdbc.JDBCDriver");
                    ds.setUrl("jdbc:hsqldb:hsql://localhost/");
                    ds.setUsername("sa");
                    ds.setPassword("");
                    return ds;
                }
                
                
                @Bean
                    public LocalSessionFactoryBean sessionFactory() {
                        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
                        sessionFactory.setDataSource(createds());
                        sessionFactory.setPackagesToScan("demo" );
                        sessionFactory.setHibernateProperties(hibernateProperties());

                        return sessionFactory;
                    }

                @Bean
                    public PlatformTransactionManager hibernateTransactionManager() {
                        HibernateTransactionManager transactionManager
                        = new HibernateTransactionManager();
                        transactionManager.setSessionFactory(sessionFactory().getObject());
                        return transactionManager;
                    }

                    private final Properties hibernateProperties() {
                        Properties hibernateProperties = new Properties();
                        hibernateProperties.setProperty( "hibernate.hbm2ddl.auto", "create");
                        hibernateProperties.setProperty( "hibernate.dialect", "org.hibernate.dialect.HSQLDialect");

                        return hibernateProperties;
                    }
                public static void main(String[] args) {
                    ApplicationContext ctx = new AnnotationConfigApplicationContext(Client5.class);
                    DeptDAO dao = ctx.getBean(DeptDAO.class);
                    Dept d = new Dept(10,"HR","Mumbai");
                    dao.create(d);

                }

            }
        Run and Complete crud operations

Lab 6 - Create new maven project (Day1.demo4.jdbc)
        pom - Spring Context, Spring jdbc
        Write demo.Client4

                @Configuration
                @ComponentScan( basePackages = "demo")
                public class Client4 {
                    //Create a datasource
                    @Bean
                    public DataSource createds() {
                        DriverManagerDataSource ds = new DriverManagerDataSource();
                        ds.setDriverClassName("org.hsqldb.jdbc.JDBCDriver");
                        ds.setUrl("jdbc:hsqldb:hsql://localhost/");
                        ds.setUsername("sa");
                        ds.setPassword("");
                        return ds;
                    }
                    
                    
                public static void main(String[] args) {
                    ApplicationContext ctx = new AnnotationConfigApplicationContext(Client4.class);
                    DeptDAO dao = ctx.getBean(DeptDAO.class);
                    dao.save(10,"aa","Pune");
                    
                }
                }
        Write demo.DeptDAO
            package demo;

            @Component
            public class DeptDAO {

                JdbcTemplate template ;
                @Autowired
                private DataSource ds;
                @PostConstruct
                public void init1() {
                    template = new JdbcTemplate(ds);
                }
                public void delete(int deptno) {
                    System.out.println("Delete invoked with deptno " + deptno );
                    String sql ="delete from dept where deptno = " + deptno;
                    System.out.println(sql);
                    template.execute(sql);
                }
                public void list() {
                    System.out.println("list invoked ");
                    String sql ="select * from dept";
                    System.out.println(sql);
                    List<Map<String,Object>> list = 	template.queryForList( sql);
                    list.stream().forEach(System.out::println);
                }
                
                public void udpate(int deptno, String newdname, String newloc) {
                    System.out.println("Update invoked with deptno " + deptno );
                    String sql ="update dept set dname='" + newdname+"' where deptno = " + deptno;
                    System.out.println(sql);
                    template.execute(sql);
                }
                public void save(int deptno, String dname, String loc) {
                    // record insertion
                    System.out.println("Save for " + deptno);
                    String sql ="insert into dept values ("+ deptno + ", '" +dname + "','" + loc + "')";
                    System.out.println(sql);
                    template.execute(sql);
                }
            }
            // create table dept (deptno numeric(2) primary key, dname varchar(20), loc varchar(20))

Lab 5  - Write simple jdbc code to connect to HSQLDb -> single operation
        Connection String -> 
Lab4 = Modify Day1.demo3.annotation  
        Write demo.Second.java  -constructor with int parameter, m1
            No Annotations 
        Run -> watch error 
        Modify Client3 to include 
            	@Bean()
                @Scope(value = "prototype")
                public Second second() {
                    System.out.println("in second method of Client 3 ");
                    return new Second(10);
                }
        Run -> with scope as singleton/prototype and check execution

        Create demo1 package   and Test.java  
                package demo1;

                import demo.First;

                @Component
                public class Test {
                    @Autowired
                    private First first;
                    
                    public void print() {
                        System.out.println("Test - Print with first "+ first);
                    }
                }
        Run -> check, remove Autowired annoation and run and check
        To see what beans are created 
          		System.out.println(Arrays.toString(applicationcontext.getBeanDefinitionNames()));

    Create an interface as Connection
        open, Close  methods
    Write two implementation classes
        OracleConnection, SQLConnection

    Write a class Dao with save method and instance of Connection interface - Authwired
        save(){
            connection.open ()
            sysout()
            connection.close()
        }
    Run and test and check messages 

Lab 3 - Day1.demo3.annotation  (copy of Lab1 project)
        no change in pom file, delete demo1.xml

        Modify first.java to include 
            @Component annotation for class
        Modify Client3.java file to 
            package demo;
            @Configuration
            @ComponentScan(basePackages = "demo")
            public class Client3 {

                public static void main(String[] args) {
                    // TODO Auto-generated method stub
                    ApplicationContext applicationcontext = new AnnotationConfigApplicationContext(Client3.class) ;
                    System.out.println("----------------------Context Loaded ------------------");
                    
                    First f1 = applicationcontext.getBean("first", First.class);
                    f1.m1();
                    First f2 = applicationcontext.getBean("first", First.class);
                    f2.m1();
                    First f3 = applicationcontext.getBean("first", First.class);
                    f3.m1();
                }
            }
        Run -> 
        Change scope and check 
        

Lab 2  - Day1.demo2.xmlanno (Copy of Lab1 project)
        demo1.xml
                <?xml version="1.0" encoding="UTF-8"?>
                <beans xmlns="http://www.springframework.org/schema/beans"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xmlns:context="http://www.springframework.org/schema/context"
                    xsi:schemaLocation="http://www.springframework.org/schema/beans
                        https://www.springframework.org/schema/beans/spring-beans.xsd
                        http://www.springframework.org/schema/context
                        https://www.springframework.org/schema/context/spring-context.xsd">

                    <context:component-scan base-package="demo"/>
                </beans>
        Run the code - observe error = "No bean named 'first' available"
        Modify first.java to include 
            @Component annotation for class
        Run code -> Singleton behaviour
        Modify first.java to include 
            @Scope(value = "prototype") 
        run code to see prototype
        Modify second.class to include both and see the output

----
Eclipse/STS -> Compiler - java8, Jre - JDK8
Lab 1 - Create a new maven project 
        Modify pom.xml to include 
            <dependencies>
                <!-- https://mvnrepository.com/artifact/org.springframework/spring-context -->
                <dependency>
                    <groupId>org.springframework</groupId>
                    <artifactId>spring-context</artifactId>
                    <version>5.3.21</version>
                </dependency>
            </dependencies>
        Check dependency hierarchy to know which files are loaded
    
        Write demo.First
            package demo;

            public class First {

                public First() {
                    System.out.println("First Contructor invoked ");
                }
                
                public void m1() {
                    System.out.println("m1 invoked ");
                }
            }
        Write demo1.xml (in resources)
                <?xml version="1.0" encoding="UTF-8"?>
                <beans xmlns="http://www.springframework.org/schema/beans"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsi:schemaLocation="http://www.springframework.org/schema/beans
                        https://www.springframework.org/schema/beans/spring-beans.xsd">
                        <bean name="first" class="demo.First" scope="prototype"> </bean>
                </beans>
        Write demo.Client1.java
                package demo;
                public class Client1 {
                    public static void main(String[] args) {
                        // TODO Auto-generated method stub
                        ClassPathXmlApplicationContext applicationcontext = new ClassPathXmlApplicationContext("demo1.xml");
                        System.out.println("----------------------Context Loaded ------------------");
                        First f1 = applicationcontext.getBean("first", First.class);
                        f1.m1();
                        First f2 = applicationcontext.getBean("first", First.class);
                        f2.m1();
                        First f3 = applicationcontext.getBean("first", First.class);
                        f3.m1();
                    }
                }
        Run
            Run the code - observe the impact of scope = prototype
            then change scope to singleton and observe difference
            Delete scope parameter in xml and observe the difference
        Write one more java file 
            demo.Second
            Modify client to include getbean for second and run 
            Modify xml


